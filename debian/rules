#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

override_dh_auto_configure:
	sed 's,@LIB@,lib/$(DEB_HOST_MULTIARCH),g' sensord-qt5.pc.in >sensord-qt5.pc
	sed 's,@LIBDIR@,/usr/lib/$(DEB_HOST_MULTIARCH),g' tests/tests.xml.in >tests/tests.xml

	# FIXME: "../../" workaround debhelper's broken -B support for qmake
	dh_auto_configure -Bdebian/build -- CONFIG+=autohybris ../../

	# HACK: CONFIG+=hybris relies on having the main packages installed. However, we want to
	# build both together in this source package, not in another source package. So, to avoid
	# having the second source package, points HIDL build to look for core libraries in the
	# normal build's build directory for the link step.
	LDFLAGS="$(LDFLAGS) -L$(PWD)/debian/build/datatypes -L$(PWD)/debian/build/core" \
		dh_auto_configure -Bdebian/build-hidl -- CONFIG+=hybris CONFIG+=binder  ../../

override_dh_auto_build:
	dh_auto_build -Bdebian/build
	dh_auto_build -Bdebian/build-hidl

# tests require packages to be installed
override_dh_auto_test:

override_dh_auto_install:
	dh_auto_install -Bdebian/build
	dh_auto_install -Bdebian/build-hidl --destdir=debian/tmp-hidl

override_dh_install:
	dh_install --no-package=libsensorfw-qt5-hidl
	dh_install --sourcedir=debian/tmp-hidl --package=libsensorfw-qt5-hidl

override_dh_installsystemd:
	dh_installsystemd -psensorfw-qt5 --name=sensorfwd

override_dh_missing:
	dh_missing --fail-missing --no-package=libsensorfw-qt5-hidl
	dh_missing --fail-missing --sourcedir=debian/tmp-hidl --package=libsensorfw-qt5-hidl

override_dh_auto_clean:
	dh_auto_clean
	rm -f sensord-qt5.pc tests/tests.xml

%:
	dh $@
